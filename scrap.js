const axios = require('axios');
let api = `https://www.zomato.com/webroutes/getPage?page_url=/kolkata/the-cake-xpress-kankurgachi`; // Found from website network tab

axios
  .get(api)
  .then(res => {
    let restaurantName = res.data.page_data.sections.SECTION_BASIC_INFO.name;
    let restaurantAddress =
      res.data.page_data.sections.SECTION_RES_CONTACT.address;
    let restaurantRating =
      res.data.page_data.sections.SECTION_BASIC_INFO.rating.rating_text;
    let restaurantVotes =
      res.data.page_data.sections.SECTION_BASIC_INFO.rating.votes;

    console.log({
      restaurantName,
      restaurantAddress,
      restaurantRating,
      restaurantVotes
    });
  })
  .catch(err => console.log(err));
